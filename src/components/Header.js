import React from 'react';
import MyRef from './images/MyRef.png'
import Group from './images/Group.png';


function Header () {

	return(
				<>
					<div className="logo">
					<div className="myreflogo">
						<img src={MyRef} alt="logo" />
					</div>
					<div className="grouplogo">	
						<img src={Group} alt="logo"/>
					</div>
				</div>
			</>
)}

export default Header;				